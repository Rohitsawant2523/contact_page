import React from 'react';
import './App.css'
import ContactPage from './component/ContactPage';

const App = () => {
  return (
    <>
      <ContactPage />
    </>
  )
}

export default App