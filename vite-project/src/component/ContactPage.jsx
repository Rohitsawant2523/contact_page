import React from 'react'
const ContactPage = () => {
    return (
        <>
            <div className='container-fluid'>
                <div className='row justify-content-center' >
                    <div className='contact_main_div col-sm-8'>
                        <div className='contact_image mt-5'>
                            <h1>D</h1>
                        </div>
                        <div className='contact_info text-center mb-3'>
                            <h5>Dinesh Hamirwasia</h5>
                            <p>Directer - NNM Group</p>
                        </div>
                        <div className='form_input'>
                            <span className='label_input'>Phone (Mobile)</span>
                            <div className='input_field' >
                                <a href="tel:+91 9702020278" className='link_text'>+91 9702020278</a>
                            </div>
                        </div>
                        <div className='form_input'>
                            <span className='label_input'>Email (Work)</span>
                            <div className='input_field'>
                                <a href="mailto:dinesh@cokaco.com" className='link_text mail_link'>dinesh@cokaco.com</a>
                            </div>
                        </div>
                        <div className='form_input'>
                            <span className='label_input'>Address</span>
                            <textarea type="text" className='input_field' rows="6" readOnly
                                defaultValue={`NNM Securities Pvt. Ltd.\nB-6/7, Shree Siddhivinayak Plaza off\nNew Link Road, Andheri-west\nMaharashtra\n400053\nIndia`}
                            >
                            </textarea>
                        </div>
                    </div>
                </div>
            </div >

        </>
    )
}

export default ContactPage